function $A(x) {
    var out = new Array(x.length);
    for(var i=0;i<x.length;i++) {
        out[i] = x[i];
    }
    return out;
}

var velocity = 127; // how hard the note hits

function MIDI() {
    return midiIFrame.contentWindow.MIDI;
}

function loadInstruments(cb) {
    var op = new XMLHttpRequest();
    op.open('get', '/instruments', true);
    op.onreadystatechange = function () {
        if(op.readyState === 4) {
            op.onreadystatechange = null;
            cb(JSON.parse(op.responseText));
        }
    };
    op.send(null);
}

function makeMIDI(path, cb) {
    createMIDI(function () {
        // THIS IS A HAAAAAAAAAAACK!!!!!
        setTimeout(function(){
        MIDI().loadPlugin({
            soundfontUrl: path,
            instrument: "acoustic_grand_piano",
            onprogress: function(state, progress) {
                console.log(state, progress);
            },
            onsuccess: function() {
                setTimeout(function(){
                MIDI().setVolume(0, 127);
                if(cb) {
                    cb();
                }}, 500);
            }
        });
        }, 500);
    });
}

var switchingCallbacks = {
    "play-instrument": function () {
        makeMIDI("/soundfont/");
        loadInstruments(function(instruments) {
            var x = document.getElementById('instrument-selector');
            x.innerHTML = "";
            var opt0 = document.createElement("option");
            opt0.value = "/soundfont/";
            opt0.textContent = "Piano";
            x.appendChild(opt0);
            instruments.forEach(function (instrument) {
                var opt = document.createElement("option");
                opt.value = "/instruments/"+instrument.UUID+"/";
                opt.textContent = instrument.Name;
                x.appendChild(opt);
            });
            x.selectedIndex = 0;
        });
    },
    "play-song": function () {
       loadInstruments(function(instruments) {
            var x = document.getElementById('instrument-selector2');
            x.innerHTML = "";
            var opt0 = document.createElement("option");
            opt0.value = "/soundfont/";
            opt0.textContent = "Piano";
            x.appendChild(opt0);
            instruments.forEach(function (instrument) {
                var opt = document.createElement("option");
                opt.value = "/instruments/"+instrument.UUID+"/";
                opt.textContent = instrument.Name;
                x.appendChild(opt);
            });
            x.selectedIndex = 0;
        });
    }
};

var midiIFrame = null;

function destroyMIDI() {
    if(midiIFrame) {
        midiIFrame.parentNode.removeChild(midiIFrame);
        midiIFrame = null;
    }
}

function createMIDI(cb) {
    destroyMIDI();
    var iframe = document.createElement('iframe');
    midiIFrame = iframe;
    iframe.onload = cb;
    iframe.className = "midi-iframe"
    document.body.appendChild(iframe);
    iframe.src = "midi-container.html";
}

function switchPane(pane) {
    destroyMIDI();
    $A(document.querySelectorAll('.pane')).forEach(function (pane) {
        pane.style.display = 'none';
    });
    document.getElementById(pane).style.display = 'block';
    if(switchingCallbacks[pane]) {
        switchingCallbacks[pane]();
    }
}

function currentKeyboardOctave() {
    var selector = document.getElementById('octave-selector');
    return parseInt(selector.options[selector.selectedIndex].value);
}

function onNoteUp(note) {
    console.log("up", note);
    MIDI().noteOn(0, note + currentKeyboardOctave(), velocity, 0);
}

function onNoteDown(note) {
    console.log("down", note);
    MIDI().noteOff(0, note + currentKeyboardOctave(), 0);
}

window.onload = function() {
    $A(document.querySelectorAll('#starting-pane button')).forEach(function (button) {
        button.onclick = function () {
            switchPane(button.getAttribute('data-dest'));
        };
    });

    $A(document.querySelectorAll('.go-back-link')).forEach(function(link) {
        link.onclick = function () {
            switchPane('starting-pane');
            return false;
        };
    });
    var is = document.getElementById('instrument-selector');
    is.onchange = function () {
        if(is.selectedIndex >= 0) {
            console.log(is.options[is.selectedIndex].value);
            makeMIDI(is.options[is.selectedIndex].value);
        }
    };
    document.getElementById("play-midi-form").onsubmit = function () {
        var file = document.querySelector('#midi-song').files[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            var x = document.getElementById("instrument-selector2");
            console.log("data url made");
            makeMIDI(x.options[x.selectedIndex].value, function (){
                var player = MIDI().Player;
                player.timeWarp = 1;
                console.log('loading file');
                player.loadFile(reader.result, player.start);
            });
        };
        if (file) {
            reader.readAsDataURL(file);
        }
        return false;
    };
}
