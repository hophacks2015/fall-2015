package main

import (
	"encoding/json"
	"github.com/jmhodges/levigo"
	"github.com/nu7hatch/gouuid"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var rootDir = filepath.Dir(os.Args[0])

var staticServer = http.FileServer(http.Dir(filepath.Join(rootDir, "static")))

func fixOutput(cmd *exec.Cmd) {
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
}

type Instrument struct {
	Name, UUID string
}

func readInstruments(db *levigo.DB) []Instrument {
	roNoCache := levigo.NewReadOptions()
	roNoCache.SetFillCache(false)
	it := db.NewIterator(roNoCache)
	out := make([]Instrument, 0, 16)
	for it.Seek([]byte("index|")); it.Valid(); it.Next() {
		key := string(it.Key())
		if !strings.HasPrefix(key, "index|") {
			break
		}
		out = append(out, Instrument{string(it.Value()), key[len("index|"):len(key)]})
	}
	defer it.Close()
	return out
}

func writeFile(tmpdir, ext string, input multipart.File) string {
	outputPath := filepath.Join(tmpdir, "input."+ext)
	f, err := os.OpenFile(outputPath, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()
	_, err = io.Copy(f, input)
	if err != nil {
		log.Panic(err)
	}
	return outputPath
}

type ProcessedAudio struct {
	OggJs, Mp3Js []byte
}

func processAudio(tmpdir, ext string, f multipart.File) *ProcessedAudio {
	defer os.RemoveAll(tmpdir)
	inputFile := writeFile(tmpdir, ext, f)
	cmd := exec.Command("python", filepath.Join(rootDir, "process_sound.py"), inputFile)
	fixOutput(cmd)
	err := cmd.Run()
	if err != nil {
		log.Panic(err)
	}
	ogg, err := ioutil.ReadFile(filepath.Join(tmpdir, "acoustic_grand_piano-ogg.js"))
	if err != nil {
		log.Panic(err)
	}
	mp3, err := ioutil.ReadFile(filepath.Join(tmpdir, "acoustic_grand_piano-mp3.js"))
	if err != nil {
		log.Panic(err)
	}
	return &ProcessedAudio{ogg, mp3}
}

func writeInstrument(name string, audio *ProcessedAudio, db *levigo.DB) {
	wo := levigo.NewWriteOptions()
	uuidObj, err := uuid.NewV4()
	if err != nil {
		log.Panic(err)
	}
	id := uuidObj.String()
	db.Put(wo, []byte("index|"+id), []byte(name))
	db.Put(wo, []byte("/instruments/"+id+"/acoustic_grand_piano-ogg.js"), audio.OggJs)
	db.Put(wo, []byte("/instruments/"+id+"/acoustic_grand_piano-mp3.js"), audio.Mp3Js)
}

func blarg(db *levigo.DB) {
	roNoCache := levigo.NewReadOptions()
	roNoCache.SetFillCache(false)
	it := db.NewIterator(roNoCache)
	for it.SeekToFirst(); it.Valid(); it.Next() {
		key := string(it.Key())
		log.Println(key)
	}
	defer it.Close()
}

func main() {
	opts := levigo.NewOptions()
	opts.SetCache(levigo.NewLRUCache(3 << 30))
	opts.SetCreateIfMissing(true)
	db, err := levigo.Open(filepath.Join(rootDir, "database"), opts)
	if err != nil {
		log.Fatal(err)
	}
	blarg(db)
	log.Fatal(http.ListenAndServe(":8080", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.Header.Del("If-Modified-Since")
		if r.RequestURI == "/" {
			http.ServeFile(w, r, filepath.Join(rootDir, "static", "index.html"))
		} else if r.RequestURI == "/instruments" {
			if r.Method == "GET" {
				instruments := readInstruments(db)
				data, err := json.Marshal(instruments)
				if err != nil {
					panic(err)
				}
				w.Header().Set("Content-Type", "application/json;charset=utf-8")
				_, err = w.Write(data)
				if err != nil {
					panic(err)
				}
			} else if r.Method == "POST" {
				name := r.FormValue("name")
				f, header, err := r.FormFile("input-file")
				if err != nil {
					log.Panic(err)
				}
				ext := filepath.Ext(header.Filename)
				tmpdir, err := ioutil.TempDir(os.TempDir(), "sound-magic")
				if err != nil {
					log.Panic(err)
				}
				log.Printf("tmpdir=%q", tmpdir)
				processed := processAudio(tmpdir, ext, f)
				writeInstrument(name, processed, db)
				w.Write([]byte("Successfully uploaded!"))
			} else {
				w.WriteHeader(504)
			}
		} else if strings.HasPrefix(r.RequestURI, "/instruments/") {
			ro := levigo.NewReadOptions()
			w.Header().Set("Content-Type", "text/javascript;charset=utf-8")
			out, err := db.Get(ro, []byte(r.RequestURI))
			log.Println(len(out))
			if err != nil {
				log.Panic(err)
			}
			_, err = w.Write(out)
			if err != nil {
				log.Panic(err)
			}
		} else {
			staticServer.ServeHTTP(w, r)
		}
	})))
}
