#!/usr/bin/env python
import sys
import os
from subprocess import check_call, check_output
import base64
import json

RUBBERBAND = "/Users/paulwatson/Downloads/rubberband-1.8.1-gpl-executable-osx-universal/rubberband"

class Pitch(object):
# frequency is in Hz
    def __init__(self, name, frequency):
        self.name = name
        self.frequency = frequency

def read_data():
    out = []
    with open(os.path.join(os.path.dirname(__file__), 'data.tsv'), 'r') as f:
        for line in f:
            parts = line.split("\t")
            out.append(Pitch(parts[0], float(parts[1])))
    return out

pitches = read_data()

def process_line(line):
    return float(line.split(" ")[1])

def average_frequency_file(x):
    data = check_output(["aubiopitch", "-i", x, "--pitch-unit=Hz"])
    lines = filter(lambda x: len(x) > 0, data.split("\n"))
    return sum(map(process_line, lines))/float(len(lines))

def dataUrlify(name):
    with open(name, "rb") as f:
        return "data:audio/" + name.split('.')[-1] + ";base64," + base64.b64encode(f.read())

def process(inputFile, tmpDir):
    truncated = os.path.join(tmpDir, "truncated.wav")
    sys.stderr.write("Removing Silence\n")
    check_call(["ffmpeg", "-i", inputFile, "-af", "silenceremove=1:0:-50dB", truncated])
    sys.stderr.write("Mixing Audio into Mono\n")
    mono = os.path.join(tmpDir, "mono.wav")
    check_call(["ffmpeg", "-i", truncated, "-ac", "1", "-ab", "192k", mono])
    sys.stderr.write("Computing Average Frequency\n")
    avg_freq = average_frequency_file(mono)
    sys.stderr.write("Average Frequency=%f\n"%avg_freq)
    mp3 = dict()
    ogg = dict()
    for pitch in pitches:
        pitch_adjusted = os.path.join(tmpDir, "%s.wav"%pitch.name)
        check_output([RUBBERBAND, "--frequency", str(pitch.frequency / avg_freq), mono, pitch_adjusted])
        check_output(["lame", "-v", "-b", "8", "-B", "64", pitch_adjusted])
        check_output(["oggenc", "-m", "32", "-M", "128", pitch_adjusted])
        mp3[pitch.name] = dataUrlify(os.path.join(tmpDir, "%s.mp3"%pitch.name))
        ogg[pitch.name] = dataUrlify(os.path.join(tmpDir, "%s.ogg"%pitch.name))
    with open(os.path.join(tmpDir, "acoustic_grand_piano-ogg.js"), "w") as f:
        f.write("""if (typeof(MIDI) === 'undefined') var MIDI = {};
if (typeof(MIDI.Soundfont) === 'undefined') MIDI.Soundfont = {};
MIDI.Soundfont.acoustic_grand_piano = %s;"""%json.dumps(ogg))
    with open(os.path.join(tmpDir, "acoustic_grand_piano-mp3.js"), "w") as f:
        f.write("""if (typeof(MIDI) === 'undefined') var MIDI = {};
if (typeof(MIDI.Soundfont) === 'undefined') MIDI.Soundfont = {};
MIDI.Soundfont.acoustic_grand_piano = %s;"""%json.dumps(mp3))


if __name__=='__main__':
    input = sys.argv[1]
    process(input, os.path.dirname(input))
